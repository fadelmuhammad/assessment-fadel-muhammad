public class SortWithoutSort {

    public static void main(String[] args) {
        int[] arr = {1, 2, 1, 3, 4, 7, 1, 1, 5, 6, 1, 8};
        bubbleSort(arr);
        cetakArray(arr);
    }

    private static void bubbleSort(int[] arr) {
        int n = arr.length;
        boolean swapped;
        for (int i = 0; i < n - 1; i++) {
            swapped = false;
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    // swap arr[j] and arr[j+1]
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    swapped = true;
                }
            }
            // Jika tidak ada pertukaran pada iterasi inner loop, array sudah terurut
            if (!swapped) {
                break;
            }
        }
    }

    private static void cetakArray(int[] arr) {
        for (int value : arr) {
            System.out.print(value + " ");
        }
        System.out.println();
    }
}

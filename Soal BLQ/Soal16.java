public class PembagianBiaya {

    public static void main(String[] args) {
        int tunaSandwich = 42;
        int spaghettiCarbonara = 50;
        int teaPitcher = 30;
        int pizza = 70;
        int salad = 30;

        // 1. Hitung total harga makanan
        int totalHargaMakanan = tunaSandwich + spaghettiCarbonara + teaPitcher + pizza + salad;

        // 2. Kurangi harga makanan Tuna Sandwich
        totalHargaMakanan -= tunaSandwich;

        // 3. Tambahkan pajak 10% dan service 5%
        double totalBiaya = totalHargaMakanan * 1.15;  // 1.10 (pajak) + 0.05 (service)

        // 4. Bagi total biaya dengan 4 untuk mendapatkan biaya per teman
        double biayaPerTeman = totalBiaya / 4;

        System.out.println("Biaya per teman: " + biayaPerTeman + "K");
    }
}

public class DonnaOlahraga {

    public static void main(String[] args) {
        int[][] konsumsi = {
            {9, 30},
            {13, 20},
            {15, 50},
            {17, 80}
        };

        int totalKalori = 0;
        for (int[] konsumsiItem : konsumsi) {
            totalKalori += konsumsiItem[1];
        }

        double totalWaktuOlahraga = 0.0;
        for (int i = 0; i < konsumsi.length - 1; i++) {
            totalWaktuOlahraga += (konsumsi[i + 1][0] - konsumsi[i][0]) * 0.1 * konsumsi[i][1] / 60.0;
        }

        // Waktu olahraga dari jam 17 ke 18
        totalWaktuOlahraga += (18 - konsumsi[konsumsi.length - 1][0]) * 0.1 * konsumsi[konsumsi.length - 1][1] / 60.0;

        // Jumlah air yang diminum setiap 30 menit
        int airPer30Menit = 100;
        int totalAir = 0;
        for (int i = 0; i < totalWaktuOlahraga * 60; i += 30) {
            totalAir += airPer30Menit;
        }

        // Air di akhir olahraga
        totalAir += 500;

        System.out.println("Total air yang diminum Donna: " + totalAir + " cc");
    }
}

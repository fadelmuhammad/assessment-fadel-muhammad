import java.time.*;

public class TarifParkir {

    private static final int TARIF_8_JAM_PERTAMA = 1000;
    private static final int TARIF_8_JAM_BERIKUTNYA = 1000;
    private static final int TARIF_24_JAM = 8000;
    private static final int TARIF_SELISIH_24_JAM = 15000;

    public static void main(String[] args) {
        hitungTarif("27 Januari 2019 | 05:00:01", "27 Januari 2019 | 17:45:03");
        hitungTarif("27 Januari 2019 | 07:03:59", "27 Januari 2019 | 15:30:06");
        hitungTarif("27 Januari 2019 | 07:05:00", "28 Januari 2019 | 00:20:21");
        hitungTarif("27 Januari 2019 | 11:14:23", "27 Januari 2019 | 13:20:00");
        hitungTarif("27 Januari 2019 | 11:14:23", "27 Januari 2019 | 13:20:00");
    }

    private static void hitungTarif(String masuk, String keluar) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy | HH:mm:ss");
        LocalDateTime waktuMasuk = LocalDateTime.parse(masuk, formatter);
        LocalDateTime waktuKeluar = LocalDateTime.parse(keluar, formatter);

        long selisihJam = ChronoUnit.HOURS.between(waktuMasuk, waktuKeluar);
        long selisihMenit = ChronoUnit.MINUTES.between(waktuMasuk, waktuKeluar) % 60;

        if (selisihJam <= 8) {
            int tarif = (int) (selisihJam * TARIF_8_JAM_PERTAMA);
            System.out.println("Tarif: " + tarif);
        } else if (selisihJam <= 24) {
            System.out.println("Tarif: " + TARIF_24_JAM);
        } else {
            int jamTambahan = (int) (selisihJam - 24);
            int tarif = TARIF_SELISIH_24_JAM + (jamTambahan * TARIF_8_JAM_BERIKUTNYA);
            System.out.println("Tarif: " + tarif);
        }
    }
}

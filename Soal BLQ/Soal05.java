public class Fibonacci {

    public static void main(String[] args) {
        int n = 10; // misalnya kita ingin menampilkan 10 bilangan Fibonacci pertama
        tampilkanFibonacci(n);
    }

    private static void tampilkanFibonacci(int n) {
        int a = 0, b = 1;

        System.out.print(a + " " + b + " "); // Menampilkan 0 dan 1 sebagai angka pertama dan kedua

        for (int i = 2; i < n; i++) {
            int c = a + b;
            System.out.print(c + " ");
            a = b;
            b = c;
        }
    }
}

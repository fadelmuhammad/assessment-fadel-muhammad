public class HattoriNinja {

    public static void main(String[] args) {
        String langkah = "N N T N N N T T T T T N T T T N T N";
        int gunung = 0;
        int lembah = 0;
        int ketinggian = 0;

        for (char langkahHattori : langkah.toCharArray()) {
            if (langkahHattori == 'N') {
                ketinggian++; // Menanjak
            } else if (langkahHattori == 'T') {
                ketinggian--; // Menurun
            }

            // Jika ketinggian kembali ke 0 dan langkah sebelumnya adalah N, itu berarti gunung
            if (ketinggian == 0 && langkahHattori == 'N') {
                gunung++;
            }

            // Jika ketinggian kembali ke 0 dan langkah sebelumnya adalah T, itu berarti lembah
            if (ketinggian == 0 && langkahHattori == 'T') {
                lembah++;
            }
        }

        System.out.println("Jumlah Gunung yang dilewati: " + gunung);
        System.out.println("Jumlah Lembah yang dilewati: " + lembah);
    }
}

import java.util.*;

public class MinMaxPenjumlahan {

    public static void main(String[] args) {
        int[] deret = {1, 2, 4, 7, 8, 6, 9};
        int minSum = cariMinSum(deret);
        int maxSum = cariMaxSum(deret);

        System.out.println("Nilai minimal penjumlahan 4 komponen: " + minSum);
        System.out.println("Nilai maksimal penjumlahan 4 komponen: " + maxSum);
    }

    private static int cariMinSum(int[] deret) {
        Arrays.sort(deret);
        int minSum = Integer.MAX_VALUE;

        for (int i = 0; i < deret.length - 3; i++) {
            int sum = deret[i] + deret[i + 1] + deret[i + 2] + deret[i + 3];
            minSum = Math.min(minSum, sum);
        }

        return minSum;
    }

    private static int cariMaxSum(int[] deret) {
        Arrays.sort(deret);
        int maxSum = Integer.MIN_VALUE;

        for (int i = 0; i < deret.length - 3; i++) {
            int sum = deret[i] + deret[i + 1] + deret[i + 2] + deret[i + 3];
            maxSum = Math.max(maxSum, sum);
        }

        return maxSum;
    }
}

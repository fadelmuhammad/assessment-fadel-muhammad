public class KonversiWaktu {

    public static void main(String[] args) {
        System.out.println(konversiFormat24Jam("03:40:44 PM"));  // Output: 15:40:44
        System.out.println(konversiFormat24Jam("10:30:15 AM"));  // Output: 10:30:15
    }

    private static String konversiFormat24Jam(String waktu12Jam) {
        String[] parts = waktu12Jam.split(":");
        int jam = Integer.parseInt(parts[0]);
        int menit = Integer.parseInt(parts[1].substring(0, 2));  // Ambil 2 karakter pertama untuk menit
        String ampm = parts[2].substring(3);  // Ambil PM atau AM dari 8 karakter terakhir

        if (ampm.equals("PM") && jam != 12) {
            jam += 12;
        } else if (ampm.equals("AM") && jam == 12) {
            jam = 0;
        }

        return String.format("%02d:%02d:%s", jam, menit, parts[2].substring(0, 2));  // Format ulang waktu
    }
}

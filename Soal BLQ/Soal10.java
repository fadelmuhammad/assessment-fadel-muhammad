public class MaskingNama {

    public static void main(String[] args) {
        String nama1 = "Susilo Bambang Yudhoyono";
        String nama2 = "Rani Tiara";

        System.out.println(maskingNama(nama1));
        System.out.println(maskingNama(nama2));
    }

    private static String maskingNama(String nama) {
        StringBuilder masked = new StringBuilder();
        String[] words = nama.split(" ");

        for (int i = 0; i < words.length; i++) {
            if (i == 0 || i == words.length - 1) {
                masked.append(words[i]).append(" ");
            } else {
                masked.append(words[i].charAt(0)).append("*** ");
            }
        }

        return masked.toString().trim();
    }
}

public class RotasiDeret {

    public static void main(String[] args) {
        int[] deret = {3, 9, 0, 7, 1, 2, 4};

        cetakArray(rotateDeret(deret.clone(), 3));  // Rotasi 3 kali
        cetakArray(rotateDeret(deret.clone(), 1));  // Rotasi 1 kali
    }

    private static int[] rotateDeret(int[] arr, int N) {
        reverse(arr, 0, arr.length - 1);            // Balik seluruh array

        reverse(arr, 0, N - 1);                     // Balik bagian pertama
        reverse(arr, N, arr.length - 1);            // Balik bagian kedua

        return arr;
    }

    private static void reverse(int[] arr, int start, int end) {
        while (start < end) {
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    private static void cetakArray(int[] arr) {
        for (int value : arr) {
            System.out.print(value + " ");
        }
        System.out.println();
    }
}

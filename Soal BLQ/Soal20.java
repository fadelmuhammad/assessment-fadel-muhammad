public class GameSuit {

    public static void main(String[] args) {
        int jarakAwal = 2;
        String[] suitA = {"Gunting", "Gunting", "Gunting"};
        String[] suitB = {"Kertas", "Kertas", "Batu"};

        String pemenang = tentukanPemenang(jarakAwal, suitA, suitB);
        System.out.println(pemenang);
    }

    public static String tentukanPemenang(int jarakAwal, String[] suitA, String[] suitB) {
        int posisiA = jarakAwal;
        int posisiB = jarakAwal;

        for (int i = 0; i < suitA.length; i++) {
            if (suitA[i].equals(suitB[i])) {
                continue; // Langkah diabaikan jika keduanya sama
            } else if (suitA[i].equals("Gunting") && suitB[i].equals("Kertas")
                    || suitA[i].equals("Kertas") && suitB[i].equals("Batu")
                    || suitA[i].equals("Batu") && suitB[i].equals("Gunting")) {
                posisiA += 2;
                posisiB -= 1;
            } else {
                posisiB += 2;
                posisiA -= 1;
            }

            if (posisiA <= 0) return "A";
            if (posisiB <= 0) return "B";
        }

        if (posisiA == posisiB) {
            return "Draw";
        } else if (posisiA > posisiB) {
            return "A";
        } else {
            return "B";
        }
    }
}

import java.util.*;

public class BelanjaAndi {

    public static void main(String[] args) {
        // Data barang dan harga
        Map<String, int[]> daftarBarang = new HashMap<>();
        daftarBarang.put("Kaca_mata", new int[]{500, 600, 700, 800});
        daftarBarang.put("Baju", new int[]{200, 400, 350});
        daftarBarang.put("Sepatu", new int[]{400, 350, 200, 300});
        daftarBarang.put("Buku", new int[]{100, 50, 150});

        // Jumlah uang yang dimiliki Andi
        int jumlahUang = 1000;

        // Menghitung kombinasi terbaik
        List<String> hasilPembelian = hitungPembelianOptimal(daftarBarang, jumlahUang);

        // Menampilkan hasil
        System.out.println("Jumlah uang yang dipakai: " + hitungTotalHarga(daftarBarang, hasilPembelian));
        System.out.println("Jumlah item yang bisa dibeli: " + hasilPembelian.size());
        System.out.println("Barang yang dibeli:");
        for (String barang : hasilPembelian) {
            System.out.println(barang);
        }
    }

    private static List<String> hitungPembelianOptimal(Map<String, int[]> daftarBarang, int jumlahUang) {
        List<String> hasilPembelian = new ArrayList<>();
        hitungPembelianOptimalHelper(daftarBarang, jumlahUang, 0, new ArrayList<>(), hasilPembelian);
        return hasilPembelian;
    }

    private static void hitungPembelianOptimalHelper(Map<String, int[]> daftarBarang, int sisaUang,
                                                      int indeksBarang, List<String> pembelianSementara,
                                                      List<String> hasilPembelian) {
        if (sisaUang < 0) {
            return;
        }

        if (sisaUang == 0) {
            if (pembelianSementara.size() > hasilPembelian.size()) {
                hasilPembelian.clear();
                hasilPembelian.addAll(pembelianSementara);
            }
            return;
        }

        for (int i = indeksBarang; i < daftarBarang.size(); i++) {
            for (int harga : daftarBarang.get(daftarBarang.keySet().toArray()[i])) {
                if (sisaUang >= harga) {
                    pembelianSementara.add(daftarBarang.keySet().toArray()[i] + " " + harga);
                    hitungPembelianOptimalHelper(daftarBarang, sisaUang - harga, i, pembelianSementara, hasilPembelian);
                    pembelianSementara.remove(pembelianSementara.size() - 1);
                }
            }
        }
    }

    private static int hitungTotalHarga(Map<String, int[]> daftarBarang, List<String> hasilPembelian) {
        int totalHarga = 0;
        for (String pembelian : hasilPembelian) {
            String[] parts = pembelian.split(" ");
            String namaBarang = parts[0];
            int harga = Integer.parseInt(parts[1]);
            totalHarga += harga;
        }
        return totalHarga;
    }
}

import java.time.*;
import java.util.*;

public class Perpustakaan {

    public static void main(String[] args) {
        Map<String, Integer> daftarBuku = new HashMap<>();
        daftarBuku.put("A", 14);
        daftarBuku.put("B", 3);
        daftarBuku.put("C", 7);
        daftarBuku.put("D", 7);

        // a. 28 Februari 2016 – 7 Maret 2016
        LocalDate start1 = LocalDate.of(2016, 2, 28);
        LocalDate end1 = LocalDate.of(2016, 3, 7);
        System.out.println("Denda (a): " + hitungDenda(daftarBuku, start1, end1));

        // b. 29 April 2018 – 30 Mei 2018
        LocalDate start2 = LocalDate.of(2018, 4, 29);
        LocalDate end2 = LocalDate.of(2018, 5, 30);
        System.out.println("Denda (b): " + hitungDenda(daftarBuku, start2, end2));
    }

    private static int hitungDenda(Map<String, Integer> daftarBuku, LocalDate start, LocalDate end) {
        int dendaTotal = 0;
        for (Map.Entry<String, Integer> entry : daftarBuku.entrySet()) {
            String namaBuku = entry.getKey();
            int durasi = entry.getValue();

            LocalDate dueDate = start.plusDays(durasi);
            if (dueDate.isBefore(end) || dueDate.isEqual(end)) {
                long daysOverdue = ChronoUnit.DAYS.between(dueDate, end);
                if (daysOverdue > 0) {
                    dendaTotal += daysOverdue * 100;
                }
            }
        }
        return dendaTotal;
    }
}

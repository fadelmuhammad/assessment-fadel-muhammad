public class SudutJarumJam {

    public static void main(String[] args) {
        System.out.println(sudutJarumJam(3, 0));   // Output: 90
        System.out.println(sudutJarumJam(5, 30));  // Output: 15
        System.out.println(sudutJarumJam(2, 20));  // Output: 50
    }

    private static int sudutJarumJam(int jam, int menit) {
        // Menghitung posisi relatif dari masing-masing jarum
        int sudutJarumJam = (jam % 12) * 30 + menit / 2;
        int sudutJarumMenit = menit * 6;

        // Menghitung perbedaan sudut
        int sudutAntaraJarum = Math.abs(sudutJarumJam - sudutJarumMenit);

        // Menentukan sudut terkecil antara kedua sudut yang mungkin
        return Math.min(sudutAntaraJarum, 360 - sudutAntaraJarum);
    }
}

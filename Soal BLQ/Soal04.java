public class BilanganPrima {

    public static void main(String[] args) {
        int n = 10; // misalnya kita ingin menampilkan 10 bilangan prima pertama
        tampilkanBilanganPrima(n);
    }

    private static void tampilkanBilanganPrima(int n) {
        int hitung = 0;
        int angka = 2; // Bilangan prima dimulai dari 2

        while (hitung < n) {
            if (apakahPrima(angka)) {
                System.out.print(angka + " ");
                hitung++;
            }
            angka++;
        }
    }

    private static boolean apakahPrima(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}

public class ManholeGame {

    public static void main(String[] args) {
        String[] lintasan1 = {"_", "_", "_", "_", "_", "O", "_", "_", "_", "Finish"};
        String[] lintasan2 = {"O", "_", "_", "_", "_", "O", "_", "_", "_", "Finish"};

        String langkah1 = tentukanLangkah(lintasan1);
        String langkah2 = tentukanLangkah(lintasan2);

        System.out.println("Jawaban 1: " + langkah1);
        System.out.println("Jawaban 2: " + langkah2);
    }

    public static String tentukanLangkah(String[] lintasan) {
        int ST = 0;
        StringBuilder langkah = new StringBuilder();

        for (int i = 0; i < lintasan.length; i++) {
            if (lintasan[i].equals("O")) {
                // Cek apakah player memiliki cukup ST untuk melompat
                if (ST >= 2) {
                    ST -= 2;
                    langkah.append("J ");
                } else {
                    return "FAILED";
                }
            } else {
                ST += 1;
                langkah.append("W ");
            }
        }

        return langkah.toString().trim();
    }
}

import java.util.*;

public class StatistikDeret {

    public static void main(String[] args) {
        int[] deret = {8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3};
        Arrays.sort(deret);

        double mean = hitungMean(deret);
        double median = hitungMedian(deret);
        int modus = hitungModus(deret);

        System.out.println("Mean: " + mean);
        System.out.println("Median: " + median);
        System.out.println("Modus: " + modus);
    }

    private static double hitungMean(int[] deret) {
        double total = 0;
        for (int num : deret) {
            total += num;
        }
        return total / deret.length;
    }

    private static double hitungMedian(int[] deret) {
        int panjang = deret.length;
        if (panjang % 2 == 0) {
            return (double) (deret[panjang / 2 - 1] + deret[panjang / 2]) / 2;
        } else {
            return deret[panjang / 2];
        }
    }

    private static int hitungModus(int[] deret) {
        Map<Integer, Integer> frekuensi = new HashMap<>();
        for (int num : deret) {
            frekuensi.put(num, frekuensi.getOrDefault(num, 0) + 1);
        }

        int modus = 0;
        int frekuensiModus = 0;
        for (Map.Entry<Integer, Integer> entry : frekuensi.entrySet()) {
            int num = entry.getKey();
            int freq = entry.getValue();
            if (freq > frekuensiModus || (freq == frekuensiModus && num < modus)) {
                modus = num;
                frekuensiModus = freq;
            }
        }
        return modus;
    }
}

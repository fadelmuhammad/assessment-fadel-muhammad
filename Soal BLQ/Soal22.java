public class MeltingCandle {

    public static void main(String[] args) {
        int[] panjangLilin = {3, 3, 9, 6, 7, 8, 23};
        
        int waktuLilinHabis = lilinHabisMeleleh(panjangLilin);
        
        System.out.println("Lilin habis meleleh pada detik ke-" + waktuLilinHabis);
    }

    public static int lilinHabisMeleleh(int[] panjangLilin) {
        int[] deretFibonacci = {1, 1};
        int detik = 2;

        // Menghitung deret Fibonacci hingga mencapai panjang lilin terpanjang
        while (deretFibonacci[deretFibonacci.length - 1] < panjangLilin.length) {
            int sum = deretFibonacci[0] + deretFibonacci[1];
            int[] temp = {deretFibonacci[1], sum};
            deretFibonacci = temp;
            detik++;
        }

        // Menghitung waktu meleleh setiap lilin
        int[] waktuMeleleh = new int[panjangLilin.length];
        for (int i = 0; i < panjangLilin.length; i++) {
            waktuMeleleh[i] = deretFibonacci[i] * panjangLilin[i];
        }

        // Mencari detik pertama di mana setidaknya satu lilin habis meleleh
        while (true) {
            for (int i = 0; i < panjangLilin.length; i++) {
                if (waktuMeleleh[i] <= 0) {
                    return detik;
                }
                waktuMeleleh[i]--;  // Mengurangi waktu meleleh setiap lilin
            }
            detik++;
        }
    }
}

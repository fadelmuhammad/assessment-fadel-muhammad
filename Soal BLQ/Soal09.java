public class PolaPerkalian {

    public static void main(String[] args) {
        cetakPolaPerkalian(3);
        cetakPolaPerkalian(4);
        cetakPolaPerkalian(5);
    }

    private static void cetakPolaPerkalian(int N) {
        for (int i = 1; i <= N; i++) {
            System.out.print(i * N + " ");
        }
        System.out.println();
    }
}

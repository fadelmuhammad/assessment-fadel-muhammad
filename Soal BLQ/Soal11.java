public class ReversePyramid {

    public static void main(String[] args) {
        cetakPyramid("Afrika");
        cetakPyramid("Jeruk");
    }

    private static void cetakPyramid(String kata) {
        int panjang = kata.length();
        
        for (int i = panjang - 1; i >= 0; i--) {
            StringBuilder baris = new StringBuilder();
            
            for (int j = 0; j < panjang; j++) {
                if (j == i) {
                    baris.append(kata.charAt(j)).append("**");
                } else {
                    baris.append("**");
                }
            }
            
            System.out.println(baris);
        }
    }
}

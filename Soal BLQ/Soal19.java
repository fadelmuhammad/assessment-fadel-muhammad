public class PangramCheck {

    public static void main(String[] args) {
        String[] kalimat = {
            "Sphinx of black quartz, judge my vow",
            "Brawny gods just flocked up to quiz and vex him",
            "Check back tomorrow; I will see if the book has arrived."
        };

        for (String s : kalimat) {
            if (isPangram(s.toLowerCase())) {
                System.out.println("\"" + s + "\" adalah pangram.");
            } else {
                System.out.println("\"" + s + "\" bukan pangram.");
            }
        }
    }

    public static boolean isPangram(String s) {
        // Menggunakan array boolean untuk melacak keberadaan huruf
        boolean[] isCharPresent = new boolean[26];

        // Memeriksa setiap karakter dalam kalimat
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);

            // Jika karakter adalah huruf alfabet
            if (ch >= 'a' && ch <= 'z') {
                isCharPresent[ch - 'a'] = true;
            }
        }

        // Memeriksa apakah semua huruf alfabet muncul
        for (boolean b : isCharPresent) {
            if (!b) {
                return false;
            }
        }

        return true;
    }
}

public class Palindrome {

    public static void main(String[] args) {
        String kata1 = "katak";
        String kata2 = "12021";
        String kata3 = "malam";
        String kata4 = "test";

        System.out.println(kata1 + " adalah palindrome: " + cekPalindrome(kata1));
        System.out.println(kata2 + " adalah palindrome: " + cekPalindrome(kata2));
        System.out.println(kata3 + " adalah palindrome: " + cekPalindrome(kata3));
        System.out.println(kata4 + " adalah palindrome: " + cekPalindrome(kata4));
    }

    private static boolean cekPalindrome(String kata) {
        int panjang = kata.length();
        for (int i = 0; i < panjang / 2; i++) {
            if (kata.charAt(i) != kata.charAt(panjang - i - 1)) {
                return false;
            }
        }
        return true;
    }
}

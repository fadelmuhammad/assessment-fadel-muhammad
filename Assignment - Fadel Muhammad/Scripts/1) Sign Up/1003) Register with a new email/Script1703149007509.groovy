import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://www.tokopedia.com/')

WebUI.click(findTestObject('Page Dashboard Tokopedia/Button Daftar'))

WebUI.setText(findTestObject('Page Daftar Tokopedia/Input Daftar Phone Number or Email'), 'a@gmail.com')

WebUI.click(findTestObject('Page Daftar Tokopedia/Button Daftar'))

WebUI.delay(3)

WebUI.verifyTextPresent('Akun Anda belum diaktivasi. Cek email Anda untuk mengaktivasi akun.', true)

WebUI.sendKeys(findTestObject('Page Daftar Tokopedia/Input Daftar Phone Number or Email'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Page Daftar Tokopedia/Input Daftar Phone Number or Email'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('Page Daftar Tokopedia/Input Daftar Phone Number or Email'), 'fadell.mhd4@gmail.com')

WebUI.click(findTestObject('Page Daftar Tokopedia/Button Daftar'))

WebUI.click(findTestObject('Page Daftar Tokopedia/Button Ya, Benar'))

WebUI.click(findTestObject('Page Daftar Tokopedia/Kode E-mail'))


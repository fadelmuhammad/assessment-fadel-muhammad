<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click X</name>
   <tag></tag>
   <elementGuidId>1ae50d17-9db5-47d7-a96b-75caabff264a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.css-12huuhu > svg.unf-icon</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>4f44ee4d-cf49-4ef6-8c37-cabad62db3be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>unf-icon</value>
      <webElementGuid>8be11515-d54d-4089-8539-31c7a08edb5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>9592815c-c194-4d49-92f5-6b9f32df3077</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>24</value>
      <webElementGuid>fd039ed8-1c83-48a7-b48c-8713565758de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>24</value>
      <webElementGuid>c613fb47-ee83-477d-90d7-1eb1bf3c5748</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fill</name>
      <type>Main</type>
      <value>var(--color-icon-enabled, #2E3137)</value>
      <webElementGuid>1756c275-bbd6-448a-918f-0798e75ec6a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;css-18e1z3z&quot;]/body[@class=&quot;css-1g5w2rw&quot;]/div[2]/div[@class=&quot;css-9u4i17 e1nc1fa20&quot;]/article[@class=&quot;css-1opgmwl-unf-modal e1nc1fa21&quot;]/header[@class=&quot;css-1hn6epg&quot;]/button[@class=&quot;css-12huuhu&quot;]/svg[@class=&quot;unf-icon&quot;]</value>
      <webElementGuid>2817d20c-4127-4e93-bc77-e7fc9794c455</webElementGuid>
   </webElementProperties>
</WebElementEntity>

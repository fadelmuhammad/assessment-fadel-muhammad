<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click (X) verifikasi</name>
   <tag></tag>
   <elementGuidId>28d8cf6d-ad0c-4a17-bcfe-4cfc129204c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.css-12huuhu > svg.unf-icon</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>9cd675c6-673f-4713-b9f9-8407568468ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>unf-icon</value>
      <webElementGuid>94f7140b-62e6-40a4-8756-f914b25a7e2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>7e47219d-bc8a-4b21-9eb3-6bbc79b175fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>24</value>
      <webElementGuid>67b918e9-8cda-4329-a293-b1c967a38405</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>24</value>
      <webElementGuid>5a12fc84-e5c3-474d-8f52-3e08f11cbd58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fill</name>
      <type>Main</type>
      <value>var(--color-icon-enabled, #2E3137)</value>
      <webElementGuid>9a163d10-5a0a-4ff9-abc2-a052f7f5d0e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;css-1g5w2rw&quot;]/div[7]/div[@class=&quot;css-y344pu e1nc1fa20&quot;]/article[@class=&quot;css-1f0x5lg-unf-modal e1nc1fa21&quot;]/header[@class=&quot;css-1hn6epg&quot;]/button[@class=&quot;css-12huuhu&quot;]/svg[@class=&quot;unf-icon&quot;]</value>
      <webElementGuid>12ec71f9-0263-44fc-98ea-fe950e8c0869</webElementGuid>
   </webElementProperties>
</WebElementEntity>

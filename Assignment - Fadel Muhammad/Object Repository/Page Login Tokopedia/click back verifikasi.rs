<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click back verifikasi</name>
   <tag></tag>
   <elementGuidId>10ffccd9-8a51-4e58-b1d5-316874da95b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//article/div/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.back-button.css-1qo5uf8-unf-link.e1u528jj0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>98a01fe2-ffb6-41bf-9bee-669d150c96f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-unify</name>
      <type>Main</type>
      <value>Link</value>
      <webElementGuid>59e76093-7a3c-4c52-9004-ac9c088a8972</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>back</value>
      <webElementGuid>726f3651-11ee-4e22-9376-f5add4d39f4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>back-button css-1qo5uf8-unf-link e1u528jj0</value>
      <webElementGuid>788d71ec-8b93-4b8a-bc8b-6d9cd080aaf7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;css-1g5w2rw&quot;]/div[7]/div[@class=&quot;css-y344pu e1nc1fa20&quot;]/article[@class=&quot;css-1f0x5lg-unf-modal e1nc1fa21&quot;]/div[@class=&quot;css-18qem4c e1nc1fa22&quot;]/div[@class=&quot;css-1qwpmq4&quot;]/a[@class=&quot;back-button css-1qo5uf8-unf-link e1u528jj0&quot;]</value>
      <webElementGuid>43ab7f91-133d-472f-a391-e3675247b8e1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//article/div/div/a</value>
      <webElementGuid>ff38105a-cefc-4df5-bce3-29fa417ab3bf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

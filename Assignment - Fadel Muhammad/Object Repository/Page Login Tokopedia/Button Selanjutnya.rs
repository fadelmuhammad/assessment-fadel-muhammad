<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Selanjutnya</name>
   <tag></tag>
   <elementGuidId>6b12ef3e-2132-47fa-a065-ab9293db3f8b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='email-phone-submit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#email-phone-submit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>fe86e678-8a5f-4707-8acf-eae616e21665</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-unify</name>
      <type>Main</type>
      <value>Button</value>
      <webElementGuid>46e543d6-5427-46d6-960f-58326b4781e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>7f254156-1adb-4e73-8b4a-21bcc9516bb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>email-phone-submit</value>
      <webElementGuid>d305b448-3207-44bf-854b-7650c1808328</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>email-phone-submit</value>
      <webElementGuid>1749ce12-7c6c-485d-98b2-356f68d62d29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-1bvc14b-unf-btn eg8apji0</value>
      <webElementGuid>d659f5c0-b12b-47da-b2bb-62ff96ee6ac0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Selanjutnya</value>
      <webElementGuid>12af3a6b-9d8d-4a98-9530-f722feb84d30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;email-phone-submit&quot;)</value>
      <webElementGuid>e901b066-0a6d-4dff-95e7-513352a6397e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='email-phone-submit']</value>
      <webElementGuid>9ba13a49-110f-4f72-851b-9fdfbb1462ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='zeus-root']/div/div[2]/section/div[2]/form/button</value>
      <webElementGuid>29330366-3a2d-4a4c-9938-eaec98ec166f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>f72d0972-06d9-4692-95d4-2317919b2123</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and @id = 'email-phone-submit' and (text() = 'Selanjutnya' or . = 'Selanjutnya')]</value>
      <webElementGuid>a0cada94-59b3-49c2-b3a0-5d26a513464c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

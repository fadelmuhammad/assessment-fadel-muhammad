<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click Daftar</name>
   <tag></tag>
   <elementGuidId>6486a16a-2074-4a31-9ca7-e5a210727967</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='zeus-root']/div/div[2]/section/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.css-2iiy0u-unf-link.e1u528jj0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5f8a809b-1a18-4e92-bcde-b04d076cf725</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-unify</name>
      <type>Main</type>
      <value>Link</value>
      <webElementGuid>26c03548-cbb5-4ab1-a10f-077f4e67b826</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>font-size</name>
      <type>Main</type>
      <value>13</value>
      <webElementGuid>e2506e06-17a1-493f-954d-0f1baf30b0ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-2iiy0u-unf-link e1u528jj0</value>
      <webElementGuid>99703f11-cb79-49ad-abb5-d972f5b2d7f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Daftar</value>
      <webElementGuid>6856d792-ecfb-484b-ba7f-e3342e776bc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;zeus-root&quot;)/div[@class=&quot;css-8atqhb&quot;]/div[@class=&quot;css-138typj&quot;]/section[@class=&quot;content css-fzw32k-unf-card eeeacht0&quot;]/div[@class=&quot;css-9z1sps&quot;]/a[@class=&quot;css-2iiy0u-unf-link e1u528jj0&quot;]</value>
      <webElementGuid>a4961cf8-b400-46d8-9d0f-ab33bfc3152b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='zeus-root']/div/div[2]/section/div/a</value>
      <webElementGuid>67144f60-1632-4e23-9a64-e22b5ed8f563</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Daftar')]</value>
      <webElementGuid>caf73e22-e78c-425e-9a19-e617429d7966</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/a</value>
      <webElementGuid>259437f5-ff26-42ff-bc82-1e26011f88d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'Daftar' or . = 'Daftar')]</value>
      <webElementGuid>2a254c01-eb5b-4d56-99ab-13a8023de7b7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

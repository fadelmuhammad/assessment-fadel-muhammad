<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Click Profile</name>
   <tag></tag>
   <elementGuidId>57b14e78-3358-446b-badd-0374f637aff5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='my-profile-header']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#my-profile-header > div.css-14ezpfl</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8ec685ca-cbb4-4f84-8b9c-af5e75f0c747</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-14ezpfl</value>
      <webElementGuid>15786081-4675-4a3f-ab78-d4ccae5731cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>fadel</value>
      <webElementGuid>ab499e66-008d-4225-9ba3-3431911e0577</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;my-profile-header&quot;)/div[@class=&quot;css-14ezpfl&quot;]</value>
      <webElementGuid>da550026-a767-4cb1-b328-941a6e2296a4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='my-profile-header']/div</value>
      <webElementGuid>cb684393-10fc-4323-aa0a-6b67dc2ef6a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div</value>
      <webElementGuid>72c535a4-e4eb-4c3c-b5b1-912a33bb3407</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'fadel' or . = 'fadel')]</value>
      <webElementGuid>aa4e1831-94a7-41c3-9521-8a556c847971</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
